import Vue from 'vue'
import store from './store'
import App from './App.vue'
import moment from './plugins/moment'
import './filters/filters.js';

Vue.prototype.$moment = moment

Vue.config.productionTip = false

import './styles/base.scss';

new Vue({
	store,
  	render: app => app(App),
}).$mount('#app')
