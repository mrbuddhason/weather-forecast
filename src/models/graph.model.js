export default class Graph {
	constructor(viewboxWidth = 500, viewboxHeight = 500) {
		this.viewboxWidth = viewboxWidth
		this.viewboxHeight = viewboxHeight
	}

	viewBox() {
		return `0 0 ${ this.viewboxWidth } ${this.viewboxHeight}`
	}
}
