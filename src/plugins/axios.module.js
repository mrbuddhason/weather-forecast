import axios from 'axios';

// Set base values for multiple api's
const axiosOpenWeather = axios.create({
  	baseURL: 'https://api.openweathermap.org'
});

// Add a request interceptor
axiosOpenWeather.interceptors.request.use(function (config) {
		const params = new URLSearchParams({
			...config.params,
			appid: process.env.VUE_APP_OPEN_WEATHER_API_KEY,
			units: process.env.VUE_APP_OPEN_WEATHER_UNITS,
			lang: process.env.VUE_APP_OPEN_WEATHER_LANG
		});

		config.params = params;

    	return config;
  	}, function (error) {
	    return Promise.reject(error);
});

export { axiosOpenWeather }
