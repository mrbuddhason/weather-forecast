import moment from 'moment'

// Settings
moment.locale(process.env.VUE_APP_OPEN_WEATHER_LANG);

export default moment
