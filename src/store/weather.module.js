import axios from 'axios';
import WeatherService from '../services/weather.service';

export const weather = {
    namespaced: true,
    state: {
        loading: false,
        city: 'Valencia',
        weatherData: null,
        activeWeatherCard: 0
    },
    actions: {
        getWeatherByCity({ dispatch, commit }, city) {
            commit('setLoadingState', true)

            return WeatherService.getWeatherByCity(city)
                .then(response => {
                    commit('setWeatherData', response.data)
                    commit('setActiveCard', 0)

                    return Promise.resolve(response);
                })
                .catch(error => {
                    console.log(error)
                    return Promise.reject(error);
                })
                .finally(() => {
                    commit('setLoadingState', false)
                })
        }
    },
    mutations: {
        setLoadingState(state, value) {
            state.loading = value
        },
        updateCity(state, value) {
            state.city = value
        },
        setWeatherData(state, value) {
            state.weatherData = value
        },
        setActiveCard(state, value) {
            state.activeWeatherCard = value
        }
    },
    getters: {
        isLoading: state => state.loading,
        city: state => state.city,
        weatherData: state => state.weatherData,
        activeWeatherCard: state => state.activeWeatherCard
    }
};
