import { axiosOpenWeather } from '../plugins/axios.module';

class WeatherService {
    getWeatherByCity(city) {
        const params = { q: city }

        return axiosOpenWeather
            .get('/data/2.5/forecast', { params })
            .then(response => {
                return Promise.resolve(response)
            })
            .catch(error => {
                return Promise.reject(error)
            })
    }
}

export default new WeatherService();
