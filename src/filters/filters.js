import Vue from 'vue';
import moment from '@/plugins/moment'

Vue.filter('toTemperature', function (value, symbol = true) {
    if (typeof value !== 'number') {
        return value;
    }

    let degreeSymbol = '&#176'

    if (symbol) {
    	degreeSymbol = '&#x2103'
    }

    let combinedValue = Math.round(value) + degreeSymbol;

    return combinedValue;
});

Vue.filter('toTime', function (value, format) {
    return moment.unix(value).utc().format(format)
});
